'use strict';

function Item () {
  this.position = createVector(random((0,width), random(0, height)));
}

Item.prototype.draw = function(){
  image(imgItem, this.position.x, this.position.y);
}

Item.prototype.eaten = function(){
  //relocate
  this.position.x = random(0, width);
  this.position.y = random(0, height);
}